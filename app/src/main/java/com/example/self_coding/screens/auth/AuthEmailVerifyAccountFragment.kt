package com.example.self_coding.screens.auth

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.self_coding.databinding.FragmentAuthEmailVerifyAccountBinding
import com.example.self_coding.services.ServiceAuth

class AuthEmailVerifyAccountFragment : Fragment() {
    private lateinit var binding: FragmentAuthEmailVerifyAccountBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentAuthEmailVerifyAccountBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onResume() {
        super.onResume()

        binding.backBtn.setOnClickListener {

        }

        binding.authBtn.setOnClickListener {
            ServiceAuth.requestVerify(email = toString(), code = Int,
                onComplete = { response ->

                },
                onApiError = { response ->

                },
                onNetworkError = { exception ->
                }
            )
        }
    }
}