package com.example.self_coding.screens.auth

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.self_coding.databinding.FragmentSignUpBinding
import com.example.self_coding.screens.utils.authReplaceFragment

class SignupFragment : Fragment() {
    private lateinit var binding: FragmentSignUpBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentSignUpBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onResume() {
        super.onResume()
        initFuns()
    }

    private fun initFuns() {
        binding.signupBtn.setOnClickListener {
          //  authReplaceFragment(NoAuthFragment(), false)
        }

        binding.authBtn.setOnClickListener {
            authReplaceFragment(AuthFragment(), false)
        }
    }
}