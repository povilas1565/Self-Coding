package com.example.self_coding.screens.auth

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.self_coding.databinding.FragmentAuthBinding
import com.example.self_coding.screens.utils.authReplaceFragment

class AuthFragment : Fragment() {
    private lateinit var binding: FragmentAuthBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentAuthBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onResume() {
        super.onResume()
        initFuns()
    }

    private fun initFuns() {
        binding.emailBtn.setOnClickListener() {
            authReplaceFragment(AuthEmailFragment(), true)
        }

        binding.googleBtn.setOnClickListener() {
           // startActivity(Intent(AUTH, MainActivity::class.java))
           // AUTH.finish();
        }
    }
}