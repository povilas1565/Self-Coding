package com.example.self_coding.screens.utils

import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import com.example.self_coding.R

fun replaceFragment(fragment: Fragment, backStack: Boolean = true) {
    APP.binding.bottomNav.visibility = View.VISIBLE
    if (backStack) {
        APP.supportFragmentManager.beginTransaction()
            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
            .replace(R.id.fragment_frame, fragment)
            .addToBackStack(null)
            .commit()
    } else {
        APP.supportFragmentManager.beginTransaction()
            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
            .replace(R.id.fragment_frame, fragment)
            .commit()
    }
}

fun mainReplaceFragment(fragment: Fragment, backStack: Boolean = true) {
    APP.binding.bottomNav.visibility = View.GONE
    if (backStack) {
        APP.supportFragmentManager.beginTransaction()
            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
            .replace(R.id.fragment_frame, fragment)
            .addToBackStack(null)
            .commit()
    } else {
        APP.supportFragmentManager.beginTransaction()
            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
            .replace(R.id.fragment_frame, fragment)
            .commit()
    }
}

fun authReplaceFragment(fragment: Fragment, backStack: Boolean = true) {
    if (backStack) {
        AUTH.supportFragmentManager.beginTransaction()
            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
            .replace(R.id.auth_frame, fragment)
            .addToBackStack(null)
            .commit()
    } else {
        AUTH.supportFragmentManager.beginTransaction()
            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
            .replace(R.id.auth_frame, fragment)
            .commit()
    }
}

fun hideBottomNav() {
    APP.binding.bottomNav.visibility = View.GONE
}
