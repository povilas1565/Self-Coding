package com.example.self_coding.screens.main

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import com.example.self_coding.R
import com.example.self_coding.databinding.ActivityMainBinding
import com.example.self_coding.screens.utils.APP
import com.example.self_coding.screens.utils.mainReplaceFragment
import com.example.self_coding.screens.utils.replaceFragment

class MainActivity : AppCompatActivity() {
    lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        initFields()
        initFuns()
    }

    private fun initFuns() {
        binding.home.setOnClickListener {
            replaceFragment(HomeFragment(), false)
            fillItem(binding.home)
        }
        binding.filter.setOnClickListener {
            replaceFragment(FilterFragment(), false)
            fillItem(binding.profile)
        }
        binding.heart.setOnClickListener {
            replaceFragment(FavouritesFragment(), false)
            fillItem(binding.heart)
        }
        binding.profile.setOnClickListener {
            replaceFragment(ProfileFragment(), false)
            fillItem(binding.profile)
        }
    }

    private fun initFields() {
        APP = this
        mainReplaceFragment(MainFragment(), false)
    }

    private fun fillItem(view: ImageView) {
        binding.home.setColorFilter(resources.getColor(R.color.gray_color))
        binding.filter.setColorFilter(resources.getColor(R.color.gray_color))
        binding.heart.setColorFilter(resources.getColor(R.color.gray_color))
        binding.profile.setColorFilter(resources.getColor(R.color.gray_color))
        view.setColorFilter(resources.getColor(R.color.background_white))
    }
}