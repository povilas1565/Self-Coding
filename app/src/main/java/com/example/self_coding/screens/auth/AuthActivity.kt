package com.example.self_coding.screens.auth

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.self_coding.databinding.ActivityAuthBinding
import com.example.self_coding.screens.utils.AUTH
import com.example.self_coding.screens.utils.authReplaceFragment

class AuthActivity : AppCompatActivity(){
    private lateinit var binding: ActivityAuthBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityAuthBinding.inflate(layoutInflater)
        setContentView(binding.root)
        initFields()
    }

    private fun initFields() {
        AUTH = this
        authReplaceFragment(SignupFragment(), false)
    }
}