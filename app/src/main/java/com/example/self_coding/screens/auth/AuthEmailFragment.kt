package com.example.self_coding.screens.auth

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.self_coding.databinding.FragmentAuthEmailBinding
import com.example.self_coding.services.ServiceAuth


class AuthEmailFragment : Fragment() {
    private lateinit var binding: FragmentAuthEmailBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentAuthEmailBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onResume() {
        super.onResume()

        binding.backBtn.setOnClickListener {

        }

        binding.btnGetCode.setOnClickListener {
           ServiceAuth.requestRegistry(
               binding.etEmail.text.toString(),
               onComplete = { response ->

               },
               onApiError = { response ->
                   if (response.isSuccess()) {
                       response.responseCode;
                   } else {
                       response.responseBody;
                   }
               },
               onNetworkError = { exception ->
               }
           )
        }

            ServiceAuth.requestVerify(
                binding.etEmail.text.toString(),
                code = Int,
                onComplete = { response ->

                },
                onApiError = { response ->

                },

                onNetworkError = { exception -> }
            )
            }
    }

