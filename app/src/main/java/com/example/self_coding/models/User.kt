package com.example.self_coding.models

import org.json.JSONObject

class User(
    val id:Int,
    val email:String,
    val isNewUser:Boolean,
    val token:String,
) {

    companion object{

        fun fromJson(json: JSONObject): User {
            return User(
                id = json.getInt("id"),
                email = json.getString("email"),
                isNewUser = json.getBoolean("is_new_user"),
                token = json.getString("token"),
            )
        }

    }



}