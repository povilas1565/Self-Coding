package com.example.self_coding.services

import com.example.self_coding.models.User
import com.example.self_coding.services.api.Request
import com.example.self_coding.services.api.Response
import org.json.JSONObject

object ServiceAuth {

    fun loadStartData(
        onComplete: () -> Unit,
        onApiError: (Response) -> Unit,
        onNetworkError: (Exception) -> Unit){

        onComplete.invoke()
    }

    fun requestRegistry(
        email:String,
        onComplete: (User) -> Unit,
        onApiError: (Response) -> Unit,
        onNetworkError: (Exception) -> Unit){

        val request = Request(
            "/v1/auth/registry",
            method = Request.methodPost,
            requestBody = JSONObject(mapOf(Pair("email", email)))
        )

        request.send(
            onComplete = { response ->
                onComplete.invoke(User.fromJson(response.getResponseData()))
            },
            onApiError = onApiError,
            onNetworkError = onNetworkError
        )
    }


    fun requestVerify(
        email:String,
        code: Int.Companion,
        onComplete: (User) -> Unit,
        onApiError: (Response) -> Unit,
        onNetworkError: (Exception) -> Unit){

        val request = Request(
            "/v1/auth/verify",
            method = "POST",
            requestBody = JSONObject(mapOf(Pair("email", email), Pair("code", code)))
        )

        request.send(
            onComplete = { response ->
                onComplete.invoke(User.fromJson(response.getResponseData()))
            },
            onApiError = onApiError,
            onNetworkError = onNetworkError
        )
    }

}