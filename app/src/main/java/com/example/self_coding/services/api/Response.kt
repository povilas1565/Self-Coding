package com.example.self_coding.services.api

import org.json.JSONObject

class Response(
    val responseCode: Int,
    val responseBody: JSONObject,
) {

    fun isSuccess(): Boolean {
        return Request.successCodes.contains(responseCode)
    }

    fun isError(): Boolean {
        return !isSuccess()
    }

    fun getResponseData(): JSONObject {
        try {
            return responseBody.getJSONObject("data")
        } catch (e: Exception) {
            return responseBody
        }
    }

    fun getResponseMessage(): String {
        try {
            return responseBody.getString("message")
        } catch (e: Exception) {
            return responseBody.toString()
        }
    }

}