package com.example.self_coding.services.api

import android.util.Log
import org.json.JSONObject
import java.io.*
import java.net.HttpURLConnection
import java.net.URL
import java.net.URLDecoder
import java.net.URLEncoder

class Request(
    val url: String,
    val params: Map<String, String> = emptyMap(),
    val method: String = methodGet,
    val requestBody: JSONObject = JSONObject(),
) {

    companion object {

        val successCodes = listOf(200, 201)
        val baseUrl = "https://self-coding.cyberia.studio/api"
        val methodPost = "POST"
        val methodGet = "GET"
        val methodDelete = "DELETE"
    }

    fun send(
        onComplete: (Response) -> Unit,
        onApiError: (Response) -> Unit,
        onNetworkError: (Exception) -> Unit
    ) {

        var paramsString = ""
        for (key in params.keys) {
            if (paramsString.isNotEmpty()) {
                paramsString += "&"
            }
            paramsString += URLEncoder.encode(key, "UTF-8") + "=" + URLEncoder.encode(
                params[key],
                "UTF-8"
            )
        }
        val url = URL(baseUrl + url + paramsString)
        val connection = url.openConnection() as HttpURLConnection

        connection.requestMethod = method
        connection.setRequestProperty("Content-Type", "application/json");
        connection.doInput = true

        Thread {
            try {

                Log.i("Requst", "=> $method $url $requestBody")

                if (method.equals(methodPost)) {
                    connection.doOutput = true
                    val out = BufferedOutputStream(connection.outputStream)
                    val writer = BufferedWriter(OutputStreamWriter(out, "UTF-8"))
                    writer.write(requestBody.toString())
                    writer.flush()
                    writer.close()
                    out.close()
                }

                connection.connect()
                val responseCode = connection.responseCode

                val responseBody = if (successCodes.contains(responseCode)) {
                    readStream(connection.inputStream)
                } else {
                    readStream(connection.errorStream)
                }

                val response = Response(responseCode, responseBody)

                connection.disconnect()

                Log.i("Requst", "<= $url $responseCode $responseBody]")

                if (response.isSuccess()) {
                    onComplete.invoke(response)
                } else {
                    onApiError.invoke(response)
                }
            } catch (e: Exception) {
                Log.e("Requst", "error", e)
                onNetworkError.invoke(e)
            }
        }.start()
    }

    fun readStream(stream: InputStream): JSONObject {
        val response = StringBuffer()
        val br = BufferedReader(InputStreamReader(stream))

        var inputLine = br.readLine()
        while (inputLine != null) {
            response.append(inputLine)
            inputLine = br.readLine()
        }

        try {

            val json = JSONObject(response.toString())

            for (key in json.keys()) {
                if (json.get(key) is String) {
                    json.put(key, URLDecoder.decode(json.get(key).toString(), "UTF-8"))
                }
            }
            return json
        } catch (e: Exception) {
            return JSONObject().put("response", response.toString())
        }

    }

}